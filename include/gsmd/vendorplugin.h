#ifndef _GSMD_VENDORPLUGIN_H
#define _GSMD_VENDORPLUGIN_H

#ifdef __GSMD__

#include <common/linux_list.h>
#include <gsmd/gsmd.h>

struct gsmd;
struct gsmd_unsolicit;

struct gsmd_vendor_plugin {
	struct llist_head list;
	char *name;
	char *ext_chars;
	unsigned int num_unsolicit;
	const struct gsmd_unsolicit *unsolicit;
	int (*detect)(struct gsmd *g);
	int (*initsettings)(struct gsmd *g);
};

extern int gsmd_vendor_plugin_load(char *name);
extern int gsmd_vendor_plugin_register(struct gsmd_vendor_plugin *pl);
extern void gsmd_vendor_plugin_unregister(struct gsmd_vendor_plugin *pl);
extern int gsmd_vendor_plugin_find(struct gsmd *g);

/* should we use AT@COPS=? instead of AT+COPS=? */
extern int g_use_ATatCOPS;
/* the last user that requested an AT@COPS=? scan */
extern struct gsmd_user *g_last_cops_user;
extern int g_last_cops_id;
struct gsmd_atcmd;
int network_opers_cb(struct gsmd_atcmd *cmd, void *ctx, char *resp);

#endif /* __GSMD__ */

#endif
